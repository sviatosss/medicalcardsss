package com.itstep.medical.repository.records.fields;

import com.itstep.medical.models.records.fields.TextField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TextFieldRepository extends JpaRepository<TextField, Long> {
    List<TextField> findTextFieldsByParent(Long id);
}