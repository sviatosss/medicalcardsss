package com.itstep.medical.repository.records.fields;

import com.itstep.medical.models.records.fields.ImgField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ImgFieldRepository extends JpaRepository<ImgField, Long> {
    List<ImgField> findImgFieldsByParent(Long id);
}