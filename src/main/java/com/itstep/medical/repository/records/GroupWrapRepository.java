package com.itstep.medical.repository.records;

import com.itstep.medical.models.records.GroupWrap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupWrapRepository extends JpaRepository<GroupWrap, Long> {
    List<GroupWrap> findGroupWrapsByStructure(Long id);
}