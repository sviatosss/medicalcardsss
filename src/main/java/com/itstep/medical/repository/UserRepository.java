package com.itstep.medical.repository;

import java.util.List;
import java.util.Optional;

import com.itstep.medical.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    List<User> findAllByActive(boolean active);

    User findUserByTelegram(String telegram);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}