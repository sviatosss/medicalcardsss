package com.itstep.medical.repository;

import java.util.Optional;

import com.itstep.medical.models.ERole;
import com.itstep.medical.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}