package com.itstep.medical.repository;

import com.itstep.medical.models.StHospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ShospitalRepository extends JpaRepository<StHospital, Long> {
}