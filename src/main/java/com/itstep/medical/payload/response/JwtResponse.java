package com.itstep.medical.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private Long user;
    private String username;
    private String email;
    private Boolean active;
    private List<String> roles;

    public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }
    public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles, Boolean active) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.active = active;
    }
    public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles, Boolean active, Long user) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.active = active;
        this.user = user;
    }
}
