package com.itstep.medical.payload.response;

import com.itstep.medical.models.records.GroupWrap;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.models.records.fields.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomRecordResponse {
    private Structure structure;

    @NotBlank
    private GroupWrap groupWrap;

    List<TextField> textField;
}