package com.itstep.medical.payload.request;

import com.itstep.medical.models.records.GroupWrap;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.models.records.fields.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomRecordRequest{
    private Structure structure;

    private GroupWrap groupWrap;

    List<TextField> textField;
}