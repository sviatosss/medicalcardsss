package com.itstep.medical.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class TelegramRequest {
    @NotBlank
    private String tUsername;

    @NotBlank
    private String tChatId;
}
