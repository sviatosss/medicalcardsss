package com.itstep.medical.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "doctors")
public class Doctor {

    @Id
    @SequenceGenerator( name = "jpaSequence", sequenceName = "JPA_SEQUENCE", allocationSize = 1)
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "jpaSequence")
    private Long id;

    private Long id_user;

    private String firstName;
    private String secondName;
    private String specialization;
    private Long phone;

    // год. прийому

    @Column(columnDefinition = "TEXT")
    private String info;


    @ManyToMany
    @JoinTable(
            name = "doctor_patients",
            joinColumns = @JoinColumn(name = "doctor"),
            inverseJoinColumns = @JoinColumn(name = "patient_id"))
    private List<Patient> doctorPatients = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "doctor", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Request> requests = new ArrayList<>();
}