package com.itstep.medical.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "patients")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long id_user;


    private String firstName;
    private String secondName;
    private String sex;
    private Long passportNum;
    private String workPlace;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthday;

    private Long phone;
    private String addressLiving;
    private String addressRegistered;

    // екстрені контакти

    @JsonBackReference
    @ManyToMany(mappedBy = "doctorPatients")
    List<Doctor> doctors = new ArrayList<>();

    @JsonManagedReference
    @OneToOne(mappedBy = "patient")
    private Card card;
}
