package com.itstep.medical.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "study_hospitals")
public class StHospital {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateFrom;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateTo;

    private String createdBy;
    private String studyInstitution;
    private String reason;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "card_id", nullable = false)
    private Card card;
}
