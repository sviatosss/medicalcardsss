package com.itstep.medical.models.records;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "field")
public class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.ORDINAL)
    private EFieldType type;

    private Integer position;

    @Column(columnDefinition = "TEXT")
    private String styles;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "structure_id", nullable = false)
    private Structure structure;
}