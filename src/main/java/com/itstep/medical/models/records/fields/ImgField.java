package com.itstep.medical.models.records.fields;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "img_fields")
public class ImgField extends BaseField {
    private String alt;
    private String url;
}