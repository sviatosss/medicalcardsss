package com.itstep.medical.models.records.fields;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "text_fields")
public class TextField extends BaseField {
    @Column(columnDefinition = "TEXT")
    private String value;
}