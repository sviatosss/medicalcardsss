package com.itstep.medical.models.records;

public enum EFieldType {
    SHORT_TEXT,
    TEXT,
    IMAGE,
    NUMERIC,
    URL
}