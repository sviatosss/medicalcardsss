package com.itstep.medical.models;

public enum ERole {
    ROLE_USER,
    ROLE_DOCTOR,
    ROLE_ADMIN
}