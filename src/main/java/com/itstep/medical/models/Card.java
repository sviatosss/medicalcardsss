package com.itstep.medical.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "cards")
public class Card {
    @Id
    private Long id;

    private String bloodGroup;

    @Column(columnDefinition = "TEXT")
    private String vaccinations;

    @Column(columnDefinition = "TEXT")
    private String allergies;

    @Column(columnDefinition = "TEXT")
    private String chronicDiseases;

    @JsonManagedReference
    @OneToMany(mappedBy = "card", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Visit> visits = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "card", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<StHospital> stHospital = new ArrayList<>();

    // історія хворіб

    @Column(columnDefinition = "TEXT")
    private String shortInfo;

    @JsonBackReference
    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;
}
