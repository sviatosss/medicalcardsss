package com.itstep.medical.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "visits")
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date date;

    private String title;
    private String reason;
    private String conclusion;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "card_id", nullable = false)
    private Card card;


    // patient

    @Column(columnDefinition = "TEXT")
    private String description;
}
