package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Card;
import com.itstep.medical.models.StHospital;
import com.itstep.medical.repository.ShospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MedicalRecordService {
    @Autowired
    private ShospitalRepository shospitalRepository;

    @Autowired
    private CardService cardService;

    public StHospital saveShospital(StHospital stHospital, Long patient_id) {
        Card card = cardService.get(patient_id);
        stHospital.setCard(card);
        return shospitalRepository.save(stHospital);
    }

    public StHospital getByIDShospital(long id) {
        return shospitalRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get medical record. ID - " + id + " doesn't exist"));
    }

}