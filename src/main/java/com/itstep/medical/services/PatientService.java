package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Card;
import com.itstep.medical.models.Doctor;
import com.itstep.medical.models.Patient;
import com.itstep.medical.repository.DoctorRepository;
import com.itstep.medical.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {
    @Autowired
    private CardService cardService;

    @Autowired
    private PatientRepository repoPatient;

    @Autowired
    private DoctorRepository repoDoctor;

    public List<Patient> getAll() {
        return repoPatient.findAll();
    }

    public Patient save(Patient patient) {
        if (patient.getCard() == null) {
            Patient patientNew = repoPatient.save(patient);
            Card emptyCard = new Card();
            emptyCard.setShortInfo("Auto generated card !");
            emptyCard.setPatient(patientNew);
            emptyCard.setId(patientNew.getId());
            Card card = cardService.add(emptyCard);
            patientNew.setCard(card);
            return patientNew;
        } else return repoPatient.save(patient);
    }

    public Patient addDoctor(Long id, Long doctor_id) {
        Patient patient = repoPatient.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get patient. ID - " + id + " doesn't exist"));
        Doctor doctor = repoDoctor.findById(doctor_id)
                .orElseThrow(() -> new IDException("Couldn't get doctor. ID - " + id + " doesn't exist"));
        List<Patient> patients = doctor.getDoctorPatients();
        patients.add(patient);
        doctor.setDoctorPatients(patients);
        repoDoctor.save(doctor);
        return patient;
//        List<Doctor> doctors = patient.getDoctors();
//        doctors.add(doctor);
//        patient.setDoctors(doctors);
//        return repoPatient.save(patient);
    }

    public Patient get(long id) {
        return repoPatient.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get patient. ID - " + id + " doesn't exist"));
    }

    public List<Doctor> getPatientDoctor(long id) {
        Patient patient = get(id);
        return patient.getDoctors();
    }

    public List<Doctor> getDoctors(long id) {
        List<Patient> patients =  getAll();
        for (Patient patient: patients) {
            if (patient.getId_user() != null) {
                if (patient.getId_user() == id) return patient.getDoctors();
            }
        }
        return new ArrayList<>();
    }

    public void delete(long id) {
        if (repoPatient.findById(id).isPresent()) {
            Patient patient = repoPatient.findById(id)
                    .orElseThrow(() -> new IDException("Couldn't get patient. ID - " + id + " doesn't exist"));
            cardService.delete(patient.getCard().getId());
            repoPatient.deleteById(id);
        } else {
            throw new IDException("Couldn't delete patient. ID - " + id + " doesn't exist");
        }
    }
}