package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Card;
import com.itstep.medical.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardService {
    @Autowired
    private CardRepository cardRepository;

    public List<Card> getAll() {
        return cardRepository.findAll();
    }

    public Card save(Card card) {
            Card cardOld = cardRepository.findById(card.getId()).orElseThrow(() ->
                    new IDException("Couldn't get card. ID - " + card.getId() + " doesn't exist"));
            cardOld.setShortInfo(card.getShortInfo());
            cardOld.setAllergies(card.getAllergies());
            cardOld.setBloodGroup(card.getBloodGroup());
            cardOld.setVaccinations(card.getVaccinations());
            cardOld.setChronicDiseases(card.getChronicDiseases());
            return cardRepository.save(cardOld);
    }

    public Card add(Card card) {
        return cardRepository.save(card);
    }

    public Card get(long id) {
        return cardRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get card. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (cardRepository.findById(id).isPresent()) {
            cardRepository.deleteById(id);
        } else {
            throw new IDException("Couldn't delete card. ID - " + id + " doesn't exist");
        }
    }
}
