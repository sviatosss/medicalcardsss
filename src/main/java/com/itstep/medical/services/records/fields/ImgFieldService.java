package com.itstep.medical.services.records.fields;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.records.fields.ImgField;
import com.itstep.medical.models.records.fields.TextField;
import com.itstep.medical.repository.records.fields.ImgFieldRepository;
import com.itstep.medical.repository.records.fields.TextFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImgFieldService {
    @Autowired
    private ImgFieldRepository imgFieldRepository;

    public List<ImgField> getAllByParent(Long id) {
        return imgFieldRepository.findImgFieldsByParent(id);
    }

    public ImgField create(ImgField imgField) {
        return imgFieldRepository.save(imgField);
    }
    public List<ImgField> createList(List<ImgField> imgFields) {
        return imgFieldRepository.saveAll(imgFields);
    }

    public ImgField get(long id) {
        return imgFieldRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get ShortText. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (imgFieldRepository.findById(id).isPresent()) {
            imgFieldRepository.deleteById(id);
        } else {
            throw new IDException("Couldn't delete Img Field. ID - " + id + " doesn't exist");
        }
    }
}
