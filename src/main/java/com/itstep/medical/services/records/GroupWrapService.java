package com.itstep.medical.services.records;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.records.GroupWrap;
import com.itstep.medical.models.records.fields.TextField;
import com.itstep.medical.payload.request.CustomRecordRequest;
import com.itstep.medical.payload.response.CustomRecordResponse;
import com.itstep.medical.repository.records.GroupWrapRepository;
import com.itstep.medical.services.records.fields.TextFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupWrapService {
    @Autowired
    private GroupWrapRepository groupWrapRepository;

    @Autowired
    private TextFieldService textFieldService;

    @Autowired
    private StructureService structureService;

    public List<GroupWrap> getAllByStructure(Long id) {
        return groupWrapRepository.findGroupWrapsByStructure(id);
    }

    public CustomRecordResponse create(CustomRecordRequest customRecordRequest, Long id) {
        GroupWrap groupWrap = customRecordRequest.getGroupWrap();
        groupWrap.setStructure(id);
        groupWrap = groupWrapRepository.save(groupWrap);

        List<TextField> fields1 =  textFieldService.createList(setParent(customRecordRequest.getTextField(), groupWrap.getId()));

        CustomRecordResponse customRecordResponse = new CustomRecordResponse();
        customRecordResponse.setTextField(fields1);
        customRecordResponse.setGroupWrap(groupWrap);

        return customRecordResponse;
    }

    private List<TextField> setParent(List<TextField> list, Long parent){
        for (TextField field: list) {
            field.setParent(parent);
        }
        return list;
    }


    public CustomRecordResponse getRecord(long id) {
        CustomRecordResponse customRecordResponse = new CustomRecordResponse();
        customRecordResponse.setGroupWrap(groupWrapRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get groupWrap. ID - " + id + " doesn't exist")));
        customRecordResponse.setTextField(textFieldService.getAllByParent(customRecordResponse.getGroupWrap().getId()));

        return customRecordResponse;
    }

    public void delete(long id) {
        if (groupWrapRepository.findById(id).isPresent()) {
            groupWrapRepository.deleteById(id);
        } else {
            throw new IDException("Couldn't delete groupWrap. ID - " + id + " doesn't exist");
        }
    }
}
