package com.itstep.medical.services.records;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Card;
import com.itstep.medical.models.records.Field;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.repository.CardRepository;
import com.itstep.medical.repository.records.StructureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StructureService {
    @Autowired
    private StructureRepository structureRepository;

    @Autowired
    private FieldService fieldService;

    public List<Structure> getAll() {
        return structureRepository.findAll();
    }

    public Structure create(Structure structure) {
        return structureRepository.save(structure);
    }

    public Structure update(Structure structure) {
        return structureRepository.save(structure);
    }

    public Structure addField(Long id, Field field){
        Structure structure = get(id);
        field.setStructure(structure);
        field = fieldService.create(field);

        List<Field> fields = structure.getFields();
        fields.add(field);
        structure.setFields(fields);
        return structureRepository.save(structure);
    }

    public Structure addFields(Long id, List<Field> allFields){
        Structure structure = get(id);
        Structure countStructure = new Structure();

        for (Field f: allFields) {
            f.setStructure(structure);
        }
        allFields = fieldService.createList(allFields);

        List<Field> fields = structure.getFields();
        fields.addAll(allFields);
        structure.setFields(fields);
        return structureRepository.save(structure);
    }

    public void deleteField(long id, long field){
        Structure structure = get(id);
        List<Field> fields = new ArrayList<>();
        for (Field f: structure.getFields()) {
            if (f.getId() != field ){
                fields.add(f);
            }
        }
        structure.setFields(fields);
        update(structure);
        fieldService.delete(field);
    }

    public Structure get(long id) {
        return structureRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get structure. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (structureRepository.findById(id).isPresent()) {
            structureRepository.deleteById(id);
        } else {
            throw new IDException("Couldn't delete structure. ID - " + id + " doesn't exist");
        }
    }
}
