package com.itstep.medical.services.records;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.records.Field;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.repository.records.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FieldService {
    @Autowired
    private FieldRepository fieldRepository;

    public List<Field> getAll() {
        return fieldRepository.findAll();
    }

    public Field create(Field field) {
        return fieldRepository.save(field);
    }

    List<Field> createList(List<Field> fields){
        return fieldRepository.saveAll(fields);
    }

    public Field get(long id) {
        return fieldRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get Field. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (fieldRepository.findById(id).isPresent()) {
            fieldRepository.deleteById(id);
        } else {
            throw new IDException("Couldn't delete Field. ID - " + id + " doesn't exist");
        }
    }
}
