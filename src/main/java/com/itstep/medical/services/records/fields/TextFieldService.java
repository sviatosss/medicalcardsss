package com.itstep.medical.services.records.fields;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.records.fields.TextField;
import com.itstep.medical.repository.records.fields.TextFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TextFieldService {
    @Autowired
    private TextFieldRepository textFieldRepository;

    public List<TextField> getAllByParent(Long id) {
        return textFieldRepository.findTextFieldsByParent(id);
    }

    public TextField create(TextField textField) {
        return textFieldRepository.save(textField);
    }
    public List<TextField> createList(List<TextField> textField) {
        return textFieldRepository.saveAll(textField);
    }

    public TextField get(long id) {
        return textFieldRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get ShortText. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (textFieldRepository.findById(id).isPresent()) {
            textFieldRepository.deleteById(id);
        } else {
            throw new IDException("Couldn't delete Text Field. ID - " + id + " doesn't exist");
        }
    }
}
