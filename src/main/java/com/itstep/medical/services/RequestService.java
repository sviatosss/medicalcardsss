package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Request;
import com.itstep.medical.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {
    @Autowired
    private RequestRepository repo;

    @Autowired
    private DoctorService doctorService;

    public List<Request> getAll() {
        return repo.findAll();
    }

    public Request save(Request request, Long id) {
        request.setDoctor(doctorService.getByID(id));
        return repo.save(request);
    }

    public Request getByID(long id) {
        return repo.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get request. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (repo.findById(id).isPresent()) {
            repo.deleteById(id);
        } else {
            throw new IDException("Couldn't delete request. ID - " + id + " doesn't exist");
        }
    }

    public List<Request> getByPatient(long id){
        return repo.findRequestsByPatient(id);
    }
}
