package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Doctor;
import com.itstep.medical.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {
    @Autowired
    private DoctorRepository repoDoctor;

    public List<Doctor> getAll() {
        return repoDoctor.findAll();
    }

    public Doctor save(Doctor doctor) {
        if (doctor.getId() != null) {
            Doctor oldDoctor = repoDoctor.findById(doctor.getId())
                    .orElseThrow(() -> new IDException("error"));
            oldDoctor.setFirstName(doctor.getFirstName());
            oldDoctor.setSecondName(doctor.getSecondName());
            oldDoctor.setSpecialization(doctor.getSpecialization());
            oldDoctor.setInfo(doctor.getInfo());
            oldDoctor.setPhone(doctor.getPhone());
            oldDoctor.setId_user(doctor.getId_user());
            return repoDoctor.save(oldDoctor);
        } else {
            return repoDoctor.save(doctor);
        }
    }

    public Doctor getByID(long id) {
        return repoDoctor.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get doctor. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (repoDoctor.findById(id).isPresent()) {
            repoDoctor.deleteById(id);
        } else {
            throw new IDException("Couldn't delete doctor. ID - " + id + " doesn't exist");
        }
    }
}