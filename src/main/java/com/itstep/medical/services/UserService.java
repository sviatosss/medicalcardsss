package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Role;
import com.itstep.medical.models.User;
import com.itstep.medical.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    private UserRepository repo;

    public List<User> getAll() {
        return repo.findAll();
    }

    public List<User> findByRole(String search) {
        String roleName = "ROLE_" + search.toUpperCase();
        List<User> allUsers = repo.findAll();
        List<User> users = new ArrayList<>();
        for (User user : allUsers) {
            Set<Role> roles = user.getRoles();
            for (Role role : roles) {
                String current = role.getName().name();
                if (current.equals(roleName)) {
                    users.add(user);
                }
            }
        }
        return users;
    }

    public String connectTChatId(String tUsername, String tChatId){
        User user = repo.findUserByTelegram(tUsername);
        if (user != null){
            user.setTChatId(tChatId);
            save(user);
        }else {
            return "https://www.frontend.com/?username="+ tUsername +"&chatId=" + tChatId;
        }
        return user.getUsername() + "! You are connected to our app !!!";
    }

    public void save(User User) {
        repo.save(User);
    }

    public User get(long id) {
        return repo.findById(id).get();
    }

    public User update(User user) {
        User userOld = repo.findById(user.getId()).orElseThrow(() -> new IDException("error"));
        userOld.setUser(user.getUser());
        userOld.setActive(user.isActive());
        return repo.save(userOld);
    }

    public void delete(long id) {
        repo.deleteById(id);
    }
}
