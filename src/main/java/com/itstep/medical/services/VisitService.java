package com.itstep.medical.services;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Card;
import com.itstep.medical.models.Visit;
import com.itstep.medical.repository.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitService {
    @Autowired
    private VisitRepository visitRepo;

    @Autowired
    private CardService cardService;

    public List<Visit> getAll() {
        return visitRepo.findAll();
    }

    public Visit save(Visit visit, Long patient_id) {
        Card card = cardService.get(patient_id);
        visit.setCard(card);
        return visitRepo.save(visit);
    }

    public Visit getByID(long id) {
        return visitRepo.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get visit. ID - " + id + " doesn't exist"));
    }

    public void delete(long id) {
        if (visitRepo.findById(id).isPresent()) {
            visitRepo.deleteById(id);
        } else {
            throw new IDException("Couldn't delete visit. ID - " + id + " doesn't exist");
        }
    }
}