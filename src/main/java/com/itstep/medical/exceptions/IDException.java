package com.itstep.medical.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IDException extends RuntimeException {
    public IDException(String message) {
        super(message);
    }
}