package com.itstep.medical.controllers;

import com.itstep.medical.models.Visit;
import com.itstep.medical.services.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/visits")
public class VisitController {

    @Autowired
    private VisitService visitService;

    @GetMapping("")
    public List<Visit> getAllVisit() {
        return visitService.getAll();
    }

    @PostMapping("/{patient_id}")
    public ResponseEntity<?> uploadVisit( @PathVariable Long patient_id, @Valid @RequestBody Visit visit) {
        Visit visitNew = visitService.save(visit, patient_id);
        return new ResponseEntity<Visit>(visitNew, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Visit> getVisitById(@PathVariable Long id) {
        Visit visit = visitService.getByID(id);
        return new ResponseEntity<Visit>(visit, HttpStatus.OK);
    }

//    @DeleteMapping("/{id}")
//    void deleteVisit(@PathVariable Long id) {
//        visitService.delete(id);
//    }
}