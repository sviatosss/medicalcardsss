package com.itstep.medical.controllers;

import com.itstep.medical.models.User;
import com.itstep.medical.payload.request.TelegramRequest;
import com.itstep.medical.repository.UserRepository;
import com.itstep.medical.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/current")
    public ResponseEntity<?> getCurrentUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Optional<User> users = userRepository.findByUsername(authentication.getName());
            return new ResponseEntity<>(users.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getProjectById(@PathVariable Long id) {
        User user = userService.get(id);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void removeUser(@PathVariable Long id) {
        userService.delete(id);
    }

    @PostMapping
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user) {
        User userNew = userService.update(user);
        return new ResponseEntity<User>(userNew, HttpStatus.OK);
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/doctor")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    public ResponseEntity<?> getAllDoctors() {
        List<User> users = userService.findByRole("doctor");
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @GetMapping("role/{role}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getProjectById(@PathVariable String role) {
        List<User> users = userService.findByRole(role);
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @GetMapping("{id}/add-telegram/{username}")
    public ResponseEntity<?> uploadDoctor(@PathVariable String username, @PathVariable Long id) {
        User user = userService.get(id);
        user.setTelegram(username);
        User response = userRepository.save(user);
        return new ResponseEntity<User>(response, HttpStatus.OK);
    }

    @PostMapping("/telegram-id")
    public ResponseEntity<?> uploadDoctor(@Valid @RequestBody TelegramRequest telegramRequest) {
        String response = userService.connectTChatId(telegramRequest.getTUsername(), telegramRequest.getTChatId());
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }
}
