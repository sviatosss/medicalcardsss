package com.itstep.medical.controllers;

import com.itstep.medical.models.StHospital;
import com.itstep.medical.services.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/medical-records")
public class MedicalRecordController {
    @Autowired
    private MedicalRecordService medRecService;

    @PostMapping("study-hospital/{patient_id}") // лікарняні
    public ResponseEntity<?> saveStudyHospital( @PathVariable Long patient_id, @Valid @RequestBody StHospital stHospital) {
        StHospital stHospitalNew = medRecService.saveShospital(stHospital, patient_id);
        return new ResponseEntity<StHospital>(stHospitalNew, HttpStatus.CREATED);
    }

    @GetMapping("study-hospital/{id}")
    public ResponseEntity<StHospital> getStudyHospitalById(@PathVariable Long id) {
        StHospital stHospital = medRecService.getByIDShospital(id);
        return new ResponseEntity<StHospital>(stHospital, HttpStatus.OK);
    }
}