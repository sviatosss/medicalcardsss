package com.itstep.medical.controllers;

import com.itstep.medical.models.Doctor;
import com.itstep.medical.models.Patient;
import com.itstep.medical.models.User;
import com.itstep.medical.security.services.UserDetailsImpl;
import com.itstep.medical.services.PatientService;
import com.itstep.medical.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/patients")
public class PatientController {
    @Autowired
    private PatientService patientService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public List<Patient> getAllPatients() {
        return patientService.getAll();
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> uploadPatient(@Valid @RequestBody Patient patient) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            patient.setId(userDetails.getId());
        }
        Patient patientNew = patientService.save(patient);
        if (patient.getId_user() != null){
            User user = userService.get(patient.getId_user());
            user.setActive(true);
            user.setUser(patientNew.getId());
            userService.save(user);
        }
        return new ResponseEntity<Patient>(patientNew, HttpStatus.CREATED);
    }

    @GetMapping(value = "{id}/doctor/{doctor_id}")
    public ResponseEntity<?> addDoctor(@Valid @PathVariable Long doctor_id, @PathVariable Long id) {
        if (id != null){
            if (doctor_id != null){
                Patient patient = patientService.addDoctor(id, doctor_id);
                return new ResponseEntity<Patient>(patient, HttpStatus.CREATED);
            }return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);
        }else return new ResponseEntity<Patient>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Patient> getPatientById(@PathVariable Long id) {
        Patient patient = patientService.get(id);
        return new ResponseEntity<Patient>(patient, HttpStatus.OK);
    }

    @GetMapping("/{id}/my-doctors")
    public ResponseEntity<?> getPatientsDoctors(@PathVariable Long id) {
        List<Doctor> doctors = patientService.getPatientDoctor(id);
        return new ResponseEntity<>(doctors, HttpStatus.OK);
    }

    @GetMapping("/{id_user}/doctors")
    public ResponseEntity<?> getDoctorsByUserId(@PathVariable Long id_user) {
        List<Doctor> doctors = patientService.getDoctors(id_user);
        return new ResponseEntity<>(doctors, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deletePatient(@PathVariable Long id) {
        patientService.delete(id);
    }
}
