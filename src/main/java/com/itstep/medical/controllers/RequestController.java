package com.itstep.medical.controllers;

import com.itstep.medical.models.Request;
import com.itstep.medical.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/requests")
public class RequestController {
    @Autowired
    private RequestService requestService;

    @PostMapping("to-doctor/{id}")
    public ResponseEntity<?> uploadRequests(@Valid @RequestBody Request request, @PathVariable Long id) {
        Request request1 = requestService.save(request, id);
        return new ResponseEntity<Request>(request1, HttpStatus.CREATED);
    }

    @GetMapping("")
    public List<Request> getAllRequest() {
        return requestService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Request> getRequestById(@PathVariable Long id) {
        Request request = requestService.getByID(id);
        return new ResponseEntity<Request>(request, HttpStatus.OK);
    }

    @GetMapping("patient/{id}")
    public ResponseEntity<?> getByPatient(@PathVariable Long id) {
        List<Request> request = requestService.getByPatient(id);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    void deleteRequest(@PathVariable Long id) {
        requestService.delete(id);
    }
}
