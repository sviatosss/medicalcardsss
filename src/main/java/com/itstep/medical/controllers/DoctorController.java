package com.itstep.medical.controllers;

import com.itstep.medical.models.Doctor;
import com.itstep.medical.models.Patient;
import com.itstep.medical.models.Request;
import com.itstep.medical.security.services.UserDetailsImpl;
import com.itstep.medical.services.DoctorService;
import com.itstep.medical.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 1)
@RestController
@RequestMapping("/doctors")
public class DoctorController {
    @Autowired
    private UserService userService;

    @Autowired
    private DoctorService doctorService;

    @GetMapping("")
    public List<Doctor> getAllDoctors() {
        return doctorService.getAll();
    }

    @GetMapping("/{id}/requests")
    public List<Request> getRequests(@PathVariable Long id) {
        return doctorService.getByID(id).getRequests();
    }

    @GetMapping("/{id}/patients")
    public List<Patient> getPatients(@PathVariable Long id) {
        return doctorService.getByID(id).getDoctorPatients();
    }

    @PostMapping("")
    public ResponseEntity<?> uploadDoctor(@Valid @RequestBody Doctor doctor) {
        Doctor doctorNew = doctorService.save(doctor);
        return new ResponseEntity<Doctor>(doctorNew, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Doctor> getDoctorById(@PathVariable Long id) {
        Doctor doctor = doctorService.getByID(id);
        return new ResponseEntity<Doctor>(doctor, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteDoctor(@PathVariable Long id) {
        doctorService.delete(id);
    }
}
