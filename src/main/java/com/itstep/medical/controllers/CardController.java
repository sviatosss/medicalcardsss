package com.itstep.medical.controllers;

import com.itstep.medical.exceptions.IDException;
import com.itstep.medical.models.Card;
import com.itstep.medical.services.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cards")
public class CardController {
    @Autowired
    private CardService cardService;

    @GetMapping("")
    public List<Card> getAllCards() {
        return cardService.getAll();
    }

    @PutMapping("")
    public ResponseEntity<?> uploadCard(@Valid @RequestBody Card card) {
        if (card.getId() != null) {
            Card cardNew = cardService.save(card);
            return new ResponseEntity<Card>(cardNew, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<RuntimeException>(new IDException("Couldn't update card. ID doesn't exist"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Card> getCardById(@PathVariable Long id) {
        Card card = cardService.get(id);
        return new ResponseEntity<Card>(card, HttpStatus.OK);
    }
}
