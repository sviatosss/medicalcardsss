package com.itstep.medical.controllers.records;

import com.itstep.medical.models.records.Field;
import com.itstep.medical.models.records.GroupWrap;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.payload.request.CustomRecordRequest;
import com.itstep.medical.payload.response.CustomRecordResponse;
import com.itstep.medical.services.records.GroupWrapService;
import com.itstep.medical.services.records.StructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/structure")
public class GroupWrapController {
    @Autowired
    private GroupWrapService groupWrapService;

    @PostMapping("/{id}/record")
    public ResponseEntity<?> addStructure(@Valid @RequestBody CustomRecordRequest customRecordRequest, @PathVariable Long id) {
        CustomRecordResponse customRecordResponse = groupWrapService.create(customRecordRequest, id);
        return new ResponseEntity<>(customRecordResponse, HttpStatus.CREATED);
    }

    @GetMapping("record/id/{id}")
    public ResponseEntity<CustomRecordResponse> getRecordsByID(@PathVariable Long id) {
        CustomRecordResponse customRecordResponse = groupWrapService.getRecord(id);
        return new ResponseEntity<>(customRecordResponse, HttpStatus.OK);
    }

    @DeleteMapping("/record/{id}")
    void deleteStructure(@PathVariable Long id) {
        groupWrapService.delete(id);
    }
}
