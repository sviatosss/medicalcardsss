package com.itstep.medical.controllers.records;

import com.itstep.medical.models.Doctor;
import com.itstep.medical.models.Patient;
import com.itstep.medical.models.Request;
import com.itstep.medical.models.records.Field;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.services.DoctorService;
import com.itstep.medical.services.MedicalRecordService;
import com.itstep.medical.services.UserService;
import com.itstep.medical.services.records.StructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/structure")
public class StructureController {
    @Autowired
    private StructureService structureService;


    @GetMapping("")
    public List<Structure> getAllStructure() {
        return structureService.getAll();
    }


    @PostMapping("")
    public ResponseEntity<?> addStructure(@Valid @RequestBody Structure structure) {
        Structure structureNew = structureService.create(structure);
        return new ResponseEntity<>(structureNew, HttpStatus.CREATED);
    }

    @PostMapping("/{id}/add-field")
    public ResponseEntity<?> addFieldToStructure(@Valid @RequestBody Field field, @PathVariable Long id) {
        Structure structureNew = structureService.addField(id, field);
        return new ResponseEntity<>(structureNew, HttpStatus.OK);
    }

    @PostMapping("/{id}/add-fields")
    public ResponseEntity<?> addFieldsToStructure(@Valid @RequestBody List<Field> fields, @PathVariable Long id) {
        Structure structureNew = structureService.addFields(id, fields);
        return new ResponseEntity<>(structureNew, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/field/{field}")
    void deleteField(@PathVariable Long id, @PathVariable Long field) {
        structureService.deleteField(id, field);
    }


    @GetMapping("/{id}")
    public ResponseEntity<Structure> getStructureById(@PathVariable Long id) {
        Structure structure = structureService.get(id);
        return new ResponseEntity<Structure>(structure, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteStructure(@PathVariable Long id) {
        structureService.delete(id);
    }
}
