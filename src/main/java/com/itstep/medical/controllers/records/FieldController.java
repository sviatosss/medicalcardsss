package com.itstep.medical.controllers.records;

import com.itstep.medical.models.records.Field;
import com.itstep.medical.models.records.Structure;
import com.itstep.medical.services.records.FieldService;
import com.itstep.medical.services.records.StructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/structure/field")
public class FieldController {
    @Autowired
    private FieldService fieldService;

    @DeleteMapping("/{id}")
    void deleteField(@PathVariable Long id) {
        fieldService.delete(id);
    }
}
